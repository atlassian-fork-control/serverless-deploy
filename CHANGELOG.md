# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.1.4

- patch: Internal maintenance: Upgrade dependency version bitbucket-pipes-toolkit.
- patch: Update the Readme with a new Atlassian Community link.

## 0.1.3

- patch: Internal maintenance: Add check for newer version.

## 0.1.2

- patch: Updated pipes toolkit version to fix coloring of log info messages.

## 0.1.1

- patch: Updated readme with GCP examples.

## 0.1.0

- minor: Initial release
- patch: Internal maintenance: update pipes toolkit version

